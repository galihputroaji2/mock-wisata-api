import Beach from './db/models/beach.js';

const db = {
    inputBeach: async (data) => {
        try {
            await Beach.input(data);
        } catch (error) {
            console.log(error);
        }
    },

    getBeaches: async () => {
        try {
            const allBeach = await Beach.findAll();
            const result = allBeach.map((beach) => beach.toJSON());
            return result;
        } catch (error) {
            console.log(error);
        }
    },

    getBeach: async (id) => {
        try {
            const beachData = await Beach.get(id);
            const result = beachData ? beachData.toJSON() : null;
            return result;
        } catch (error) {
            console.error(error.message);
            return "Error getting data";
        }
    },
};

export { db };
