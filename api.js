import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { db } from './functions.js';

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.get('/beaches', async (req, res) => {
    try {
        const beaches = await db.getBeaches();
        res.json(beaches)
    } catch (error) {
        res.status(500).json({ error: 'Failed to fetch beach data' });
    }
});

app.listen(process.env.PORT || 3002, () => console.log('Example app listening on port 3002!'));
