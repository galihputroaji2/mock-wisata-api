import { Sequelize, DataTypes } from 'sequelize';

const db = new Sequelize({
    dialect: 'sqlite',
    storage: 'database.sqlite',
    // logging: (...msg) => console.log(msg)
});

const Beach = db.define('Beach', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    nama: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    alamat: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    map_url: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    cover: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    photos: {
        type: DataTypes.JSON, // Use JSON data type for photos
        allowNull: false,
        get() {
            // Convert photos from JSON string to array
            const photos = this.getDataValue('photos');
            return photos ? JSON.parse(photos) : [];
        },
        set(value) {
            // Convert photos from array to JSON string
            this.setDataValue('photos', JSON.stringify(value));
        },
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
});

// Method input
Beach.input = async (data) => {
    try {
        const beach = await Beach.create(data);
        return beach;
    } catch (error) {
        throw new Error('Failed to input beach data.');
    }
};

// Method get
Beach.get = async (id) => {
    try {
        const beach = await Beach.findByPk(id);
        if (!beach) throw new Error('Beach not found.');
        return beach;
    } catch (error) {
        throw new Error('Failed to get beach data.');
    }
};

// Method edit
Beach.edit = async (id, data) => {
    try {
        const beach = await Beach.findByPk(id);
        if (!beach) throw new Error('Beach not found.');
        await beach.update(data);
        return beach;
    } catch (error) {
        throw new Error('Failed to edit beach data.');
    }
};

// Method delete
Beach.delete = async (id) => {
    try {
        const beach = await Beach.findByPk(id);
        if (!beach) throw new Error('Beach not found.');
        await beach.destroy();
        return true;
    } catch (error) {
        throw new Error('Failed to delete beach data.');
    }
};

export default Beach;
