'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('Beaches', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            nama: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            alamat: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            map_url: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            cover: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            photos: {
                type: Sequelize.JSON, // Gunakan JSON untuk menyimpan array JSON
                allowNull: false,
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Beaches');
    }
};
